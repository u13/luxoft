package com.usarskyi.luxoft.task1

import org.junit.Assert.*
import org.junit.Test


class MainTest {

    private val main = MaxSumCalculator()

    @Test
    fun testCalc() {
        assertEquals(0, main.calculateMaxSumOfNonAdjacents(emptyArray()))

        assertEquals(3, main.calculateMaxSumOfNonAdjacents(arrayOf(3)))

        assertEquals(5, main.calculateMaxSumOfNonAdjacents(arrayOf(3, 5)))

        assertEquals(23, main.calculateMaxSumOfNonAdjacents(arrayOf(7, 10, 10, 10, 6)))

        assertEquals(32, main.calculateMaxSumOfNonAdjacents(arrayOf(8, 10, 8, 10, 8, 10, 8)))

        assertEquals(34, main.calculateMaxSumOfNonAdjacents(arrayOf(8, 10, 8, 10, 8, 9, 10)))

        assertEquals(30, main.calculateMaxSumOfNonAdjacents(arrayOf(8, 10, 8, 10, 8, 10)))

        assertEquals(35, main.calculateMaxSumOfNonAdjacents(arrayOf(8, 10, 8, 9, 10, 10, 9)))

        assertEquals(13, main.calculateMaxSumOfNonAdjacents(arrayOf(3, 10, 9, 0, 1)))

        assertEquals(29, main.calculateMaxSumOfNonAdjacents(arrayOf(1, 10, 1, 10, 9, 9, 5)))

        assertEquals(29, main.calculateMaxSumOfNonAdjacents(arrayOf(10, 1, 1, 10, 9, 9, 5)))

        assertEquals(30, main.calculateMaxSumOfNonAdjacents(arrayOf(10, 1, 1, 10, 8, 8, 10)))

        assertEquals(9, main.calculateMaxSumOfNonAdjacents(arrayOf(5, 7, 4)))

        assertEquals(25, main.calculateMaxSumOfNonAdjacents(arrayOf(10, 9, 8, 7, 1, 7)))
    }
}