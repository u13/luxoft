package com.usarskyi.luxoft.task2

import org.junit.Assert.*
import org.junit.Before
import org.junit.Test


class CompanyTest {

    private lateinit var company: Company

    @Before
    fun setUp() {
        company = Company()
    }

    @Test
    fun `Add CEO to empty company`() {
        try {
            val employee = company.addEmployee(createCEO())
            assertNotNull(company.findEmployeeById(employee.id))
        } catch (e: Exception) {
            println(e)
            fail("Exception should not be thrown")
        }
    }

    @Test
    fun `Add CEO with boss id to empty company throws exception`() {
        try {
            company.addEmployee(createCEO().copy(currentBossId = 1))
        } catch (e: Exception) {
            println(e)
            return
        }
        fail("Exception should be thrown")
    }

    @Test
    fun `Add CEO twice throws exception`() {
        company.addEmployee(createCEO())
        try {
            company.addEmployee(createCEO())
        } catch (e: Exception) {
            println(e)
            return
        }
        fail("Exception should be thrown")
    }

    @Test
    fun `Add Manager with no boss id throws exception`() {
        try {
            company.addEmployee(createManager().copy(currentBossId = null))
        } catch (e: Exception) {
            println(e)
            return
        }
        fail("Exception should be thrown")
    }

    @Test
    fun `Add Manager with non-existing boss id throws exception`() {
        try {
            company.addEmployee(createManager().copy(currentBossId = 4))
        } catch (e: Exception) {
            println(e)
            return
        }
        fail("Exception should be thrown")
    }

    @Test
    fun `Add Manager after CEO`() {
        val ceoId = company.addEmployee(createCEO()).id
        try {
            company.addEmployee(createManager().copy(currentBossId = ceoId))
        } catch (e: Exception) {
            println(e)
            fail("Exception should not be thrown")
        }
    }

    @Test
    fun `Add Manager with manager boss throws exception`() {
        val ceoId = company.addEmployee(createCEO()).id
        val managerId = company.addEmployee(createManager().copy(currentBossId = ceoId)).id
        try {
            company.addEmployee(createManager().copy(currentBossId = managerId))
        } catch (e: Exception) {
            println(e)
            return
        }
        fail("Exception should be thrown")
    }

    @Test
    fun `Delete non-existing employee throws exception`() {
        try {
            company.deleteEmployee(10)
        } catch (e: Exception) {
            println(e)
            return
        }
        fail("Exception should be thrown")
    }

    @Test
    fun `Delete employee who is a boss throws exception`() {
        val ceoId = company.addEmployee(createCEO()).id
        val managerId = company.addEmployee(createManager().copy(currentBossId = ceoId)).id
        company.addEmployee(createOfficeWorker(managerId)).id
        try {
            company.deleteEmployee(managerId)
        } catch (e: Exception) {
            println(e)
            return
        }
        fail("Exception should be thrown")
    }

    @Test
    fun `Delete employee`() {
        val ceoId = company.addEmployee(createCEO()).id
        val managerId = company.addEmployee(createManager().copy(currentBossId = ceoId)).id
        val officeWorkerId = company.addEmployee(createOfficeWorker(managerId)).id
        try {
            company.deleteEmployee(officeWorkerId)
        } catch (e: Exception) {
            println(e)
            fail("Exception should not be thrown")
        }
    }

    // TODO: add tests for update and find


    private fun createCEO(): Employee {
        return Employee(
            id = 0,
            name = "John",
            age = 40,
            address = "qwerqweq",
            position = Position.CEO,
            currentReports = "qqq",
            currentBossId = null
        )
    }

    private fun createManager(): Employee {
        return Employee(
            id = 0,
            name = "Mary",
            age = 41,
            address = "qwerqweq",
            position = Position.MANAGER,
            currentReports = "qqq",
            currentBossId = 1
        )
    }

    private fun createOfficeWorker(bossId: Long): Employee {
        return Employee(
            id = 0,
            name = "Peter",
            age = 41,
            address = "qwerqweq",
            position = Position.OFFICE_WORKER,
            currentReports = "qqq",
            currentBossId = bossId
        )
    }
}