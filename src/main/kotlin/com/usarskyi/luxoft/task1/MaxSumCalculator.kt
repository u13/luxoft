package com.usarskyi.luxoft.task1

import kotlin.math.max


class MaxSumCalculator {
    
    fun calculateMaxSumOfNonAdjacents(arr: Array<Int>): Int {
        if (arr.size < 2) {
            return arr.sum()
        }
        var maxSumIncludingItemI = arr[0]
        var maxSumExcludingItemI = 0

        for (i in 1 until arr.size) {
            val currentMaxSum = max(maxSumIncludingItemI, maxSumExcludingItemI)
            maxSumIncludingItemI = maxSumExcludingItemI + arr[i]
            maxSumExcludingItemI = currentMaxSum
        }
        
        return max(maxSumIncludingItemI, maxSumExcludingItemI)
    }
}
