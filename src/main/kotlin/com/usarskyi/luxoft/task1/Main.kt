package com.usarskyi.luxoft.task1

private val TEST_ARR = arrayOf(7, 10, 10, 10, 6)

fun main() {
    // sample usage of max sum calculator
    val main = MaxSumCalculator()
    val result = main.calculateMaxSumOfNonAdjacents(TEST_ARR)
    println(result)
}