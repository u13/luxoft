package com.usarskyi.luxoft.task2

import java.lang.IllegalArgumentException
import kotlin.math.max

class Company {

    private val employees = mutableMapOf<Long, Employee>()

    /**
     * Adds a new employee.
     *
     * @param employee Employee to add. Please note that employee.id will be ignored.
     * @return a new employee object
     */
    fun addEmployee(employee: Employee): Employee {
        if (employee.position.priority == 0) { // check the main position
            if (employee.currentBossId != null) {
                throw IllegalArgumentException("${employee.position} cannot have a boss id assigned")
            }
            // only one main position in a company
            employees.values.find { it.position == employee.position }?.let {
                throw IllegalArgumentException("${employee.position} already exists")
            }
        } else {
            if (employee.currentBossId == null) {
                throw IllegalArgumentException("Employee must have a boss id assigned")
            }
            val boss = findEmployeeById(employee.currentBossId)
                ?: throw IllegalArgumentException("Boss Employee with a given id=${employee.currentBossId} does not exist")
            if (boss.position.priority >= employee.position.priority) {
                throw IllegalArgumentException("Boss cannot have position lower or equal to employee")
            }
        }
        val nextId = (employees.keys.takeIf { it.isNotEmpty() }?.reduce { acc, id -> max(acc, id) } ?: 0) + 1
        return employee.copy(id = nextId).also {
            employees[nextId] = it
        }
    }

    fun deleteEmployee(employeeId: Long) {
        findEmployeeById(employeeId) ?: throw IllegalArgumentException("Employee with id=$employeeId not found")
        if (findEmployeesByBossId(employeeId).isNotEmpty()) {
            throw IllegalArgumentException("Cannot delete employee who is assigned as a boss to other employees")
        }
        employees.remove(employeeId)
    }

    fun updateEmployee(employee: Employee) {
        val dbEmployee =
            findEmployeeById(employee.id) ?: throw IllegalArgumentException("Employee with id=${employee.id} not found")
        if (dbEmployee.position.priority == 0) {
            if (dbEmployee.position != dbEmployee.position) {
                throw IllegalArgumentException("Cannot change ${dbEmployee.position} position")
            }
            if (employee.currentBossId != null) {
                throw IllegalArgumentException("Cannot set ${dbEmployee.position} boss")
            }
        } else {
            if (employee.position != dbEmployee.position) {
                if (employee.position.priority == 0) {
                    throw IllegalArgumentException("Cannot change position to ${employee.position}")
                } else {
                    if (findEmployeesByBossId(employee.id).isNotEmpty()) {
                        throw IllegalArgumentException("Cannot change position if employee is assigned as a boss of other employees")
                    }
                }
            }
            if (employee.currentBossId != dbEmployee.currentBossId) {
                if (employee.currentBossId == null) {
                    throw IllegalArgumentException("Boss id must be assigned")
                } else {
                    val boss = findEmployeeById(employee.currentBossId)
                        ?: throw IllegalArgumentException("Boss Employee with a given id=${employee.currentBossId} does not exist")
                    if (boss.position.priority >= employee.position.priority) {
                        throw IllegalArgumentException("Boss cannot have position lower or equal to employee")
                    }
                }
            }
        }
        employees[employee.id] = employee
    }

    fun findEmployeeByName(name: String): List<Employee> {
        return employees.values.filter { it.name == name }
    }

    fun findEmployeeById(id: Long): Employee? {
        return employees[id]
    }

    fun findEmployeesByBossId(id: Long): List<Employee> {
        return employees.values.filter { it.currentBossId == id }
    }
}