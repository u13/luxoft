package com.usarskyi.luxoft.task2

data class Employee(
    val id: Long,
    val name: String,
    val age: Int,
    val address: String,
    val position: Position,
    val currentReports: Any,
    val currentBossId: Long?
)

enum class Position(val priority: Int) {
    CEO(0),
    MANAGER(10),
    OFFICE_WORKER(20)
}
